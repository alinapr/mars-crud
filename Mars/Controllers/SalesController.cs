﻿using Mars.Models;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace Mars.Controllers
{
    public class SalesController : Controller
    {
        private MarsEntities db = new MarsEntities();

        // GET: Sales
        public ActionResult Index()
        {
            return View();
        }

        
        public JsonResult GetDetails()
        {
            if (db.ProductSold != null)
            {
                var customers = db.Customers.Select(c => new { c.Id, c.Name }).ToList();
                var products = db.Products.Select(p => new { p.Id, p.Name }).ToList();
                var stores = db.Stores.Select(s => new { s.Id, s.Name }).ToList();

                List<Object> array = new List<Object>(){ customers, products, stores };
                return Json( array, JsonRequestBehavior.AllowGet);
            }                
            return Json(new { Success= false }, JsonRequestBehavior.DenyGet);
        }


        [HttpPost]
        public ActionResult AddSale(ProductSold prodsold)
        {
            if (prodsold != null)
            {
                
                var idcustomer = db.Customers.Where(user => user.Id == prodsold.CustomerId).Select(col => col.Id).Single();
                var idproduct = db.Products.Where(prod => prod.Id == prodsold.ProductId).Select(col => col.Id).Single();
                var idstore = db.Stores.Where(sto => sto.Id == prodsold.StoreId).Select(col => col.Id).Single();
                string text_date = Request.Form["date"]; 
                DateTime date = Convert.ToDateTime(text_date); 
                

                if (idcustomer > 0 && idproduct > 0  && idstore > 0) //if ID exists then add sale
                {
                    var q = db.ProductSold.Add(new ProductSold() {
                        CustomerId = Convert.ToInt32(idcustomer),
                        ProductId = Convert.ToInt32(idproduct),
                        StoreId = Convert.ToInt32(idstore),
                        DateSold = date,
                    });
                    db.SaveChanges();
                    return Json("SUCCESS", JsonRequestBehavior.AllowGet);
                }
              
            }
            return Json("FAILED", JsonRequestBehavior.DenyGet);
        }


        public JsonResult DeleteSale(int saleId)
        {
            var sale = db.ProductSold.Where(s => s.Id == saleId).Single();
            if (sale != null)
            {
                db.ProductSold.Remove(sale);
                db.SaveChanges();
                return Json("SUCCESS", JsonRequestBehavior.AllowGet);
            }
            return Json("FAILED", JsonRequestBehavior.AllowGet);
        }

       
        public JsonResult GetAll()
        {
            db.Configuration.LazyLoadingEnabled = false; 
            var result = (dynamic)null;
            if (db.ProductSold.Any())
            {
                result = db.ProductSold.Select(col => new
                {
                    Id = col.Id,
                    Customer = col.Customer,
                    Product = col.Product,
                    Store = col.Store,
                    DateSold = col.DateSold
                }).ToList();
                db.Dispose();
                return Json(result, JsonRequestBehavior.AllowGet);
            }
            return Json("FAILED", JsonRequestBehavior.DenyGet);
        }


        public JsonResult UpdateSale(ProductSold prodsold)
        {

            if (ModelState.IsValid)
            {
                try 
                {
                    var q = db.ProductSold.Where(user => user.Id == prodsold.Id).Select(col => new { col.CustomerId, col.ProductId, col.StoreId, col.DateSold }).Single();
                    string text_date = Request.Form["date"]; 
                    DateTime date = Convert.ToDateTime(text_date); 

                    q = new {
                        CustomerId = prodsold.CustomerId,
                        ProductId = prodsold.ProductId,
                        StoreId = prodsold.StoreId,
                        DateSold = date
                    };
                    db.Entry(prodsold).State = EntityState.Modified; 
                    db.SaveChanges();
                    return Json(new { Success = true }, JsonRequestBehavior.AllowGet);
                }
                catch (Exception e)
                {
                    return Json(new { Success = false, e }, JsonRequestBehavior.DenyGet);
                }
            }
            return Json(new { Success = false }, JsonRequestBehavior.DenyGet);
            }
    }
}