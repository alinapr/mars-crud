﻿using Mars.Models;
using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace Mars.Controllers
{
    public class ProductsController : Controller
    {
        private MarsEntities db = new MarsEntities();

        // GET: Products
        public ActionResult Index()
        {
            return View();
        }

        // JSON
        public JsonResult GetProductDetails()
        {
            if (db.Products != null)
                return Json(db.Products.ToList(), JsonRequestBehavior.AllowGet);
            return Json(db.Products.ToList(), JsonRequestBehavior.DenyGet);
        }


        public JsonResult AddProduct(Product product)
        {
            if (ModelState.IsValid) // if all fields are completed, adds product
            {
                var query = db.Products.Add(new Product() { Name = product.Name, Price = product.Price  });
                db.SaveChanges();
                return Json(db.Products.ToList(), JsonRequestBehavior.AllowGet);
            }
            return Json(db.Products.ToList(), JsonRequestBehavior.DenyGet);
        }

        public JsonResult UpdateProduct(Product product)
        {

            if (ModelState.IsValid)
            {
                try
                {
                    var q = db.Products.Where(prdct => prdct.Id == prdct.Id).Select(c => new { c.Name, c.Price }).Single();
                    q = new { product.Name, product.Price };
                    db.Entry(product).State = EntityState.Modified; 
                    db.SaveChanges();
                    return Json(db.Products.ToList(), JsonRequestBehavior.AllowGet);
                }
                catch (Exception e)
                {
                    Console.WriteLine(e);
                }
            }
            return Json(db.Products.ToList(), JsonRequestBehavior.DenyGet);
        }


        public JsonResult DeleteProduct(int productId)
        {
            var deletedProduct = from product in db.Products
                         join prodsold in db.ProductSold on product.Id equals prodsold.ProductId
                         where product.Id == productId && prodsold.ProductId == productId
                         select prodsold;

            foreach (var record in deletedProduct)
            {
                db.ProductSold.Remove(record);
            }


            var p = db.Products.Where(prod => prod.Id == productId).Single();
            db.Products.Remove(p);

            db.SaveChanges();
            return Json(db.Products.ToList(), JsonRequestBehavior.AllowGet);
        }
    }
}