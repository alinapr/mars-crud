﻿using Mars.Models;
using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Web;
using System.Web.Mvc;


namespace Mars.Controllers
{
    public class CustomersController : Controller
    {
        private MarsEntities db = new MarsEntities();

        // GET: Customer
        public ActionResult Index()
        {
            return View();
        }


       
        public JsonResult GetCustomersDetails()
        {
            if (db.Customers != null)
                return Json(db.Customers.ToList(), JsonRequestBehavior.AllowGet);
            return Json(db.Customers.ToList(), JsonRequestBehavior.DenyGet);
        }

        
        public JsonResult AddCustomer(Customer customer)
        {
            if (ModelState.IsValid) // checking the fields are completed
            {
                var q = db.Customers.Add(new Customer() { Name = customer.Name, Address = customer.Address });
                db.SaveChanges();
                return Json(db.Customers.ToList(), JsonRequestBehavior.AllowGet);
            }
            return Json(db.Customers.ToList(), JsonRequestBehavior.DenyGet);
        }

        public JsonResult UpdateCustomer(Customer customer)
        {
            
            if (ModelState.IsValid)
            {
                try
                {
                    var query = db.Customers.Where(user => user.Id == customer.Id).Select(c => new { c.Name , c.Address}).Single();
                    query = new {customer.Name, customer.Address};
                    db.Entry(customer).State = EntityState.Modified; //updates the entity
                    db.SaveChanges();
                    return Json(db.Customers.ToList(), JsonRequestBehavior.AllowGet);
                }
                catch (Exception e)
                {
                    Console.WriteLine(e);
                }                
            }
            return Json(db.Customers.ToList(), JsonRequestBehavior.DenyGet);
        }

        
        public JsonResult DeleteCustomer(int customerId)
        {              
            var delete = from customer in db.Customers
                           join prodsold in db.ProductSold on customer.Id equals prodsold.CustomerId
                           where customer.Id == customerId && prodsold.CustomerId == customerId
                           select prodsold;

            foreach (var record in delete)
            {
                db.ProductSold.Remove(record);
            }

            var deletedCustomer = db.Customers.Where(user => user.Id == customerId).Single(); //find the record
            
            db.Customers.Remove(deletedCustomer); //delete the record

            db.SaveChanges(); //save changes

            return Json(db.Customers.ToList(), JsonRequestBehavior.AllowGet);
        }

    }
}